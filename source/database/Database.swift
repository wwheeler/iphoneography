//
//  Database.swift
//  iLo
//
//  Created by Garrick Michael Wheeler on 2016/01/03.
//  Copyright © 2016 Seagram Pearce. All rights reserved.
//

import Foundation
import CoreData

public class Database {
    
    public class categories {
        public static var PARENT_PARENT_ID = -10.0
        
        private static var TABLE_KEY = "Categories"
        private static var TABLE_NAME_KEY = "name"
        
        // check entry exists
        public static func exist(managedObjectContext:NSManagedObjectContext, categoryName:String) throws -> Bool {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.predicate = NSPredicate(format: "name == %@", categoryName)
            
            var error:NSError? = nil
            let count = managedObjectContext.countForFetchRequest(fetchRequest, error:&error)
            if count > 0 {
                return true
            }
            return false
        }
        
        // to insert an entry
        public static func insert(managedObjectContext:NSManagedObjectContext, name:String) throws -> Bool {
            let nextID = Double(getNextId(managedObjectContext))
            let entityDescription = NSEntityDescription.entityForName(TABLE_KEY, inManagedObjectContext: managedObjectContext)
            let categoryObj = CategoryObject(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
            
            categoryObj.thisId = nextID
            categoryObj.name = name
            
            do {
                try managedObjectContext.save()
                return true
            } catch {
                print(error as NSError)
            }
            return false
        }
        
        // delete a row by ID
        public static func deleteById(managedObjectContext:NSManagedObjectContext, categoryId:Double) throws -> Bool {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.predicate = NSPredicate(format: "thisId == %f", categoryId)
            
            do {
                let categories = try managedObjectContext.executeFetchRequest(fetchRequest) as! [CategoryObject]
                if let entityToDelete = categories.first {
                    managedObjectContext.deleteObject(entityToDelete)
                }
            } catch {
                print(error as NSError)
            }
            do {
                try managedObjectContext.save()
                return true
            } catch {
                print(error as NSError)
            }
            return false
        }
        
        // fetch all
        public static func selectAll(managedObjectContext:NSManagedObjectContext) throws -> [CategoryObject]  {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: TABLE_NAME_KEY, ascending: true)]
            return try managedObjectContext.executeFetchRequest(fetchRequest) as! [CategoryObject]
        }
        
        // fetch a row by ID
        public static func selectById(managedObjectContext:NSManagedObjectContext, categoryId:Double) throws -> CategoryObject {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.predicate = NSPredicate(format: "thisId == %f", categoryId)
            return try (managedObjectContext.executeFetchRequest(fetchRequest) as! [CategoryObject])[0]
        }
        
        // fetch all child rows by ID
        public static func selectAllChildrenByParentId(managedObjectContext:NSManagedObjectContext, parentCategoryId:Double) throws -> [CategoryObject] {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: TABLE_NAME_KEY, ascending: true)]
            fetchRequest.predicate = NSPredicate(format: "parentcategoryId == %f", parentCategoryId)
            return try managedObjectContext.executeFetchRequest(fetchRequest) as! [CategoryObject]
        }
        
        // fetch all parent categories
        public static func selectAllParentCategories(managedObjectContext:NSManagedObjectContext) throws -> [CategoryObject] {
            return try selectAllChildrenByParentId(managedObjectContext, parentCategoryId: PARENT_PARENT_ID)
        }
        
        // get next availableID
        private static func getNextId(managedObjectContext:NSManagedObjectContext) -> Int {
            do {
                let rows = try selectAll(managedObjectContext)
                return findNextId(rows, nextInt: 0);
            } catch {
                print(error as NSError)
            }
            return -2
        }
        
        // a recursive loop to get the next ID from this table
        private static func findNextId(dataRows:[CategoryObject], nextInt:Int) -> Int {
            var nextIdValue = nextInt
            if(dataRows.capacity > 0) {
                for category in dataRows {
                    if(category.thisId == Double(nextIdValue)) {
                        nextIdValue += 1
                        return findNextId(dataRows, nextInt: nextIdValue)
                    }
                }
                return nextIdValue;
            }
            return 0
        }
    }
    
    public class locations {
        static var TABLE_KEY = "Locations"
        
        // check entry exists
        public static func exist(managedObjectContext:NSManagedObjectContext, categoryId:Double, locationName:String) throws -> LocationObject? {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.predicate = NSPredicate(format: "name == %@", locationName)
            
                        
            return try (managedObjectContext.executeFetchRequest(fetchRequest).first as? LocationObject)
// ??????????????? location name can have many categories
//            var error:NSError? = nil
//            let count = managedObjectContext.countForFetchRequest(fetchRequest, error:&error)
//            if count > 0 {
//                return true
//            } else {
//                return false
//            }
        }
        
        // to insert an entry
        public static func insert(managedObjectContext:NSManagedObjectContext, categoryId:Double, name:String, location:String, date:NSDate, comment:String) -> Bool {
            let entityDescription = NSEntityDescription.entityForName(TABLE_KEY, inManagedObjectContext: managedObjectContext)
            let locationObj = LocationObject(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext);

            locationObj.date = date
            locationObj.categoryId = categoryId
            locationObj.name = name
            locationObj.location = location
            locationObj.comment = comment
            
            do {
                try managedObjectContext.save()
                return true
            } catch {
                print(error as NSError)
                return false
            }
        }
        
        // to update an entry
        public static func update(managedObjectContext:NSManagedObjectContext, categoryId:Double, name:String, location:String, date:NSDate, comment:String) -> Bool {
            do {
                let locationObj = try self.selectByLocationName(managedObjectContext, locationName:name)
                
                locationObj.date = date
                locationObj.categoryId = categoryId
                locationObj.name = name
                locationObj.location = location
                locationObj.comment = comment
                
                do {
                    try managedObjectContext.save()
                    return true
                } catch {
                    print(error as NSError)
                    return false
                }
            } catch {
                print(error as NSError)
                return false
            }
        }
        
        // delete a row by ID
        public static func deleteByLocationName(managedObjectContext:NSManagedObjectContext, locationName:String) throws -> Bool {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.predicate = NSPredicate(format: "name == %@", locationName)
            
            do {
                let locations = try managedObjectContext.executeFetchRequest(fetchRequest) as! [LocationObject]
                if let entity = locations.first {
                    managedObjectContext.deleteObject(entity)
                }
            } catch {
                return false
            }
            do {
                try managedObjectContext.save()
                return true
            } catch {
                print(error as NSError)
                return false
            }
        }
        
        // fetch all
        public static func selectAll(managedObjectContext:NSManagedObjectContext) throws -> [LocationObject]  {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            return try managedObjectContext.executeFetchRequest(fetchRequest) as! [LocationObject]
        }
        
        // fetch all by category id
        public static func selectByCategoryId(managedObjectContext:NSManagedObjectContext, categoryId:Double) throws -> [LocationObject] {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.predicate = NSPredicate(format: "categoryId == %f", categoryId)
            return try (managedObjectContext.executeFetchRequest(fetchRequest) as! [LocationObject])
        }
        
        // fetch all by location name
        public static func selectByLocationName(managedObjectContext:NSManagedObjectContext, locationName:String) throws -> LocationObject {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.predicate = NSPredicate(format: "name == %@", locationName)
            return try (managedObjectContext.executeFetchRequest(fetchRequest)[0] as! LocationObject)
        }
        
        public static func selectAllByLocationName(managedObjectContext:NSManagedObjectContext, locationName:String) throws -> [LocationObject] {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.predicate = NSPredicate(format: "name == %@", locationName)
            return try managedObjectContext.executeFetchRequest(fetchRequest) as! [LocationObject]
        }
    }
    
    public class images {
        static var TABLE_KEY = "Images"
        
        // to insert an entry
        public static func insert(managedObjectContext:NSManagedObjectContext, name:String, locationName:String) -> Bool {
            let entityDescription = NSEntityDescription.entityForName(TABLE_KEY, inManagedObjectContext: managedObjectContext)
            let imagesObj = ImageObject(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext);
            
            imagesObj.name = name;
            imagesObj.locationName = locationName;
            
            do {
                try managedObjectContext.save()
                return true
            } catch {
                print(error as NSError)
                return false
            }
        }
        
        // fetch all
        public static func selectAll(managedObjectContext:NSManagedObjectContext) throws -> [ImageObject]  {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            return try managedObjectContext.executeFetchRequest(fetchRequest) as! [ImageObject]
        }
        
        // fetch a row by ID
        public static func selectByLocationName(managedObjectContext:NSManagedObjectContext, locationName:String) throws -> [ImageObject] {
            let fetchRequest = NSFetchRequest(entityName: TABLE_KEY)
            fetchRequest.predicate = NSPredicate(format: "locationName == %@", locationName)
            return try (managedObjectContext.executeFetchRequest(fetchRequest) as! [ImageObject])
        }
    }
}
