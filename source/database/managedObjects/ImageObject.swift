//
//  ImageObject.swift
//  iLo
//
//  Created by Garrick Michael Wheeler on 2016/01/12.
//  Copyright © 2016 Seagram Pearce. All rights reserved.
//

import Foundation
import CoreData

@objc(ImageObject)
public class ImageObject:NSManagedObject {
    @NSManaged var name:String
    @NSManaged var locationName:String
}
