//
//  NavigationHelper.swift
//  iLo
//
//  Created by Garrick Michael Wheeler on 2016/01/03.
//  Copyright © 2016 Seagram Pearce. All rights reserved.
//

import Foundation
import UIKit

public class NavigationHelper {
    
    // usage : NavigationHelper.presentWithBack("mapViewController", parentView: self);
    static func presentWithBack(className:String, parentView:UIViewController) {
        dispatch_async(dispatch_get_main_queue(), {
            parentView.presentViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier(className), animated: true, completion: nil)
        })
    }
    
    // usage : NavigationHelper.dismiss(self)
    static func dismiss(parentView:UIViewController) {
        dispatch_async(dispatch_get_main_queue(), {
            parentView.dismissViewControllerAnimated(true, completion: nil)
        })
    }
}
