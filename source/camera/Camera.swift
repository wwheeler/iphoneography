//
//  Camera.swift
//  iLo
//
//  Created by wWheeler on 2015/12/23.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import UIKit
import AVFoundation

class Camera: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var blutEffect: UIVisualEffectView!
    @IBOutlet weak var background: UIView! // vu d'affichage

    var previewLayer = AVCaptureVideoPreviewLayer()
    var captureSession = AVCaptureSession()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCamera()
        //        camera()
    }
    
    @IBAction func takePhoto(sender: AnyObject) {
        captureSession.stopRunning()
    }
    
    func loadCamera() {
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerControllerSourceType.Camera) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerControllerSourceType.Camera
                imagePicker.mediaTypes = [kUTTypeImage as NSString]
                imagePicker.allowsEditing = false
                
                self.presentViewController(imagePicker, animated: true, 
                    completion: nil)
                newMedia = true
        }
    }
    
    func camera () {
        captureSession = AVCaptureSession()
        previewLayer = AVCaptureVideoPreviewLayer()
        var captureDevice : AVCaptureDevice?
        let devices = AVCaptureDevice.devices()
        captureSession.sessionPreset = AVCaptureSessionPresetHigh
        
//        background.layer.sublayers?.removeAll()
        
        
        for device in devices {
            
            if (device.hasMediaType(AVMediaTypeVideo)) {
                
                if(device.position == AVCaptureDevicePosition.Back) {
                    captureDevice = device as? AVCaptureDevice
                    if captureDevice != nil {
                        
                        do {
                            try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
                        } catch _ as NSError {
                            print("ERROR")
                        }
                        
                        
                        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                        self.background.layer.addSublayer(previewLayer)
                        previewLayer.frame = self.background.layer.frame
                        captureSession.startRunning()
                    }
                }
            }
        }
    }
    
    func screenShotMethod() {
        UIGraphicsBeginImageContext(background.layer.frame.size)
//        background.layer.renderInContext(UIGraphicsGetCurrentContext())
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //Save it to the camera roll
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    }
}
