//
//  CamViewController.swift
//  iLo
//
//  Created by wWheeler on 2015/12/23.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import UIKit

class CamViewController: UIViewController {
    
    @IBOutlet var cameraView:CameraView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        self.cameraView =
        cameraView.init
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sliderChanged(sender: UISlider) {
        cameraView.setFocusWithLensPosition(sender.value)
    }
}


