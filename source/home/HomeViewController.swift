//
//  ViewController.swift
//  iLo
//
//  Created by Garrick Wheeler on 2015/09/18.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import UIKit
import MobileCoreServices

class HomeViewController: ParentViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var libraryButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var icon_o: UIImageView!
    @IBOutlet weak var icon_l: UIImageView!
    @IBOutlet weak var icon_i: UIImageView!
    
    var parallaxStrength = 20
    var iconParallaxStrength = 5
    var controller: UIImagePickerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backGroundParallax()
        iconParallax(icon_i, amount: iconParallaxStrength)
        iconParallax(icon_l, amount: iconParallaxStrength)
        iconParallax(icon_o, amount: iconParallaxStrength)
    }
    
    // MARK:- LifeCycle Methods
    
    override func viewDidAppear(animated: Bool) {
        // lighten the status bar
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        StaticValuesHelper.resetAllObj()
        LibraryAPI.createLibraryFolder()
        
        libraryButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cameraButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationBarHidden(true);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: AnyObject]){
        dispatch_async(dispatch_get_main_queue(), {
            picker.dismissViewControllerAnimated(false, completion: nil)
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                StaticValuesHelper.setImage(pickedImage.orientationCorrection())
                NavigationHelper.presentWithBack("locationViewController", parentView:self);
            }
        })
    }
    
    // MARK:- Button Methods
    
    func handleButtonActions(sender:UIButton!) {
        if sender == libraryButton {
            StaticValuesHelper.setReadOnly()
            NavigationHelper.presentWithBack("libraryViewController", parentView: self);
        } else if sender == cameraButton {
            StaticValuesHelper.setReadAndWrite()
            self.openCamera()
        }
    }
    
    // MARK:- Camera Methods
    
    func openCamera() {
        if !CameraHelper.isCameraAvailable() {
            CameraHelper.presentAlertViewController(self)
        } else {
            CameraHelper.openCamera(self, delegate: self)
        }
    }
    
    // MARK:- Parallax Methods
    
    func backGroundParallax() {
        // controls the parallax effect of the background
        // Set vertical effect
        let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y",
                                                               type: .TiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = parallaxStrength * -1
        verticalMotionEffect.maximumRelativeValue = parallaxStrength
        
        // Set horizontal effect
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x",
                                                                 type: .TiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = parallaxStrength * -1
        horizontalMotionEffect.maximumRelativeValue = parallaxStrength
        
        // Create group to combine both
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontalMotionEffect, verticalMotionEffect]
        
        // Add both effects to your view
        backgroundImageView.addMotionEffect(group)
    }
    
    func iconParallax(imageView: UIView, amount: Int) {
        // first we do the "i"
        // Set vertical effect
        let verticalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.y",
                                                               type: .TiltAlongVerticalAxis)
        verticalMotionEffect.minimumRelativeValue = amount
        verticalMotionEffect.maximumRelativeValue = amount * -1
        
        // Set horizontal effect
        let horizontalMotionEffect = UIInterpolatingMotionEffect(keyPath: "center.x",
                                                                 type: .TiltAlongHorizontalAxis)
        horizontalMotionEffect.minimumRelativeValue = amount
        horizontalMotionEffect.maximumRelativeValue = amount * -1
        
        // Create group to combine both
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontalMotionEffect, verticalMotionEffect]
        
        // Add both effects to your view
        imageView.addMotionEffect(group)
    }
}

extension UIImage {
    func orientationCorrection() -> UIImage {
        if (self.imageOrientation == UIImageOrientation.Up) {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransformIdentity
        
        if (self.imageOrientation == UIImageOrientation.Down ||
            self.imageOrientation == UIImageOrientation.DownMirrored) {
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height)
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI))
        }
        if (self.imageOrientation == UIImageOrientation.Left ||
            self.imageOrientation == UIImageOrientation.LeftMirrored) {
            transform = CGAffineTransformTranslate(transform, self.size.width, 0)
            transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2))
        }
        if (self.imageOrientation == UIImageOrientation.Right ||
            self.imageOrientation == UIImageOrientation.RightMirrored) {
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform,  CGFloat(-M_PI_2));
        }
        if (self.imageOrientation == UIImageOrientation.UpMirrored ||
            self.imageOrientation == UIImageOrientation.DownMirrored) {
            transform = CGAffineTransformTranslate(transform, self.size.width, 0)
            transform = CGAffineTransformScale(transform, -1, 1)
        }
        if (self.imageOrientation == UIImageOrientation.LeftMirrored ||
            self.imageOrientation == UIImageOrientation.RightMirrored) {
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
        }
        
        let ctx: CGContextRef = CGBitmapContextCreate(nil, Int(self.size.width), Int(self.size.height),
                                                      CGImageGetBitsPerComponent(self.CGImage), 0,
                                                      CGImageGetColorSpace(self.CGImage),
                                                      CGImageGetBitmapInfo(self.CGImage).rawValue)!;
        CGContextConcatCTM(ctx, transform)
        
        if (self.imageOrientation == UIImageOrientation.Left ||
            self.imageOrientation == UIImageOrientation.LeftMirrored ||
            self.imageOrientation == UIImageOrientation.Right ||
            self.imageOrientation == UIImageOrientation.RightMirrored) {
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage)
        } else {
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage)
        }
        
        return UIImage(CGImage: CGBitmapContextCreateImage(ctx)!)
    }
}
