//
//  ThisLocation.swift
//  iLo
//
//  Created by Garrick Wheeler on 2015/12/29.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import Foundation
import UIKit

public class ThisLocation {
    
    static private var saved = false
    static private var mId:Double?
    static private var mName:String?
    static private var mCategoryId:Double?
    static private var mLocation:String?
    static private var mDate:NSDate?
    static private var mComment:String?
    static private var mImages:[UIImage] = []
    
    static func reset() {
        print("reset")
        self.mId = nil
        self.mName = nil
        self.mCategoryId = nil
        self.mLocation = nil
        self.mDate = nil
        self.mComment = nil
        self.saved = false
        //        self.images = []
    }
    
    static func setObject(location:LocationObject) {
        print("setObject : ", location.thisId, " : ", location.name, " : ",  location.categoryId)
        self.mId = location.thisId
        self.mName = location.name
        self.mCategoryId = location.categoryId
        self.mLocation = location.location
        self.mDate = location.date
        self.saved = true
    }
    
    static func isSaved() -> Bool { return saved }
    static func canSave() -> Bool {
        if(self.mName == nil) {
            return false
        } else if(self.mName == "") {
            return false
        } else if(self.mCategoryId == nil) {
            return false
        } else if(self.mLocation == nil) {
            return false
        } else {
            return true
        }
    }
    
    //  setters
    static func setId(input:Double) { self.mId = input }
    static func setName(input:String) { self.mName = input }
    static func setCategoryId(input:Double) { self.mCategoryId = input }
    static func setLocation(input:String) { self.mLocation = input }
    static func setDate(input:NSDate) { self.mDate = input }
    static func setComment(input:String) { self.mComment = input }
    static func setImages(input:[UIImage]) { self.mImages = input }
    
    //  getters
    static func getId() -> Double! { return self.mId }
    static func getName() -> String! { return self.mName }
    static func getCategoryId() -> Double! { return self.mCategoryId }
    static func getLocation() -> String! { return self.mLocation }
    static func getDate() -> NSDate? {
        if(self.mDate == nil) {
            self.mDate = NSDate()
        }
        return self.mDate
    }
    
    static func getComment() -> String? {
        if(self.mComment == nil) {
            return ""
        }
        return self.mComment
    }
    
    
    // Image Methods
    static func getFirstImage() -> UIImage { return getImageForIndex(0) }
    static func getImageCount() -> Int{ return mImages.count }
    static func getImageForIndex(index:Int) -> UIImage {
        return mImages[index]
    }
    static func addImage(imageInput:UIImage) {
        mImages.append(imageInput)
    }
}