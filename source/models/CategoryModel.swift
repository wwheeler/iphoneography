//
//  CategoryModel.swift
//  iLo
//
//  Created by wWheeler on 2016/03/08.
//  Copyright © 2016 Company. All rights reserved.
//

import UIKit

class CategoryModel {
    
    var thisId:Double = -1
    var name:String = ""
    
    var categoryObj:CategoryObject!
    
    func initWithObject(object:CategoryObject) {
        self.thisId = object.thisId
        self.name = object.name
        
        self.categoryObj = object
    }
}