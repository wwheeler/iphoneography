//
//  LocationModel.swift
//  iLo
//
//  Created by wWheeler on 2016/03/08.
//  Copyright © 2016 Company. All rights reserved.
//

import UIKit

class LocationModel {
    
    var date:NSDate = NSDate()
    var categoryId:Double = -1
    var name:String = ""
    var location:String = ""
    var comment:String = ""
    
    var locationObj:LocationObject!
    
    func initWithObject(object:LocationObject) {
        self.date = object.date
        self.categoryId = object.categoryId
        self.name = object.name
        self.location = object.location
        self.comment = object.comment
        
        self.locationObj = object
    }
}