//
//  ViewController.swift
//  TEST
//
//  Created by wWheeler on 2015/12/15.
//  Copyright © 2015 company. All rights reserved.
//

import UIKit

class ParentViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigationBarHidden(isHidden:Bool) {
        navigationBarCustomColor();
        self.navigationController?.navigationBarHidden = isHidden;
    }
    
    func navigationBarCustomColor() {
        let darkGrey = UIColor(red: 28/255, green: 28/255, blue: 28/255, alpha: 1.0)
        
        self.navigationController?.navigationBar.barTintColor = darkGrey
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    }
}