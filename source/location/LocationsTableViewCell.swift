//
//  LocationsTableViewCell.swift
//  iLo
//
//  Created by wwheeler on 2016/04/24.
//  Copyright © 2016 Company. All rights reserved.
//

import UIKit

public class LocationsTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var label: UILabel!
}