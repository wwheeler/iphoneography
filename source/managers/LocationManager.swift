//
//  LocationManager.swift
//  iLo
//
//  Created by wWheeler on 2015/12/30.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import CoreLocation

class LocationManager:CLLocationManager {
    
    // MARK: - Authorization
    
    private func requestLocationAuthorization() {
        self.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
//            self.delegate = self
            self.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
    }
    
    func saveLocation(location:CLLocation) {
        // TODO save location to core data
        
    }
    
    func getAndSaveLocation() {
        // TODO save picture location to core data
        
//        self.delegate = self
        self.requestLocation()
        
//        let location = 
    }
}