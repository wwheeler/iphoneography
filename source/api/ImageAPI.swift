//
//  DataAPI.swift
//  iLo
//
//  Created by wWheeler on 2016/03/09.
//  Copyright © 2016 Company. All rights reserved.
//

import CoreData

class ImageAPI: NSObject {
    
    static func save(managedObjectContext:NSManagedObjectContext, imageModel:ImageModel) {
        Database.images.insert(managedObjectContext, name:imageModel.name, locationName:imageModel.locationName)
    }
    
    static func getFirstImagePathByLocationName(managedObjectContext:NSManagedObjectContext, locationName:String) -> String {
        if let path = self.getImagesPathsByLocationName(managedObjectContext, locationName:locationName).first {
            return path
        }
        return ""
    }
    
    static func getImagesPathsByLocationName(managedObjectContext:NSManagedObjectContext, locationName:String) -> [String] {
        let libraryPath = LibraryAPI.libraryPath
        var paths:[String] = []
        
        let images = self.getImagesByLocationName(managedObjectContext, locationName:locationName)
        for item in images {
            paths.append(libraryPath! + "/" + item.name)
        }
        return paths
    }
    
    static func getImagesByLocationName(managedObjectContext:NSManagedObjectContext, locationName:String) -> [ImageObject]! {
        do {
            return try Database.images.selectByLocationName(managedObjectContext, locationName:locationName)
        } catch {
            print(error as NSError)
        }
        return nil
    }
    
    static func getFirstImagePathsByLocationNamesWithCategoryId(managedObjectContext:NSManagedObjectContext, id:double_t) -> [String] {
        var newPaths:[String] = []
        let locations =  LocationAPI.getLocationsByCategoryId(managedObjectContext, id:id)
        for location in locations {
            let path = self.getFirstImagePathByLocationName(managedObjectContext, locationName:location.name)
            newPaths.append(path)
        }
        return newPaths
    }
}