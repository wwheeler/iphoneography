//
//  DataAPI.swift
//  iLo
//
//  Created by wWheeler on 2016/03/09.
//  Copyright © 2016 Company. All rights reserved.
//

import CoreData

class CategoryAPI: NSObject {
    
    static func save(managedObjectContext:NSManagedObjectContext, categoryName:String) -> Bool {
        do {
            if (self.checkCategoryExistsByCategoryName(managedObjectContext, categoryName:categoryName) == false)  {
                return try Database.categories.insert(managedObjectContext, name:categoryName)
            }
        } catch {
            print(error as NSError)
        }
        return false
    }
    
    static func deleteByCategoryId(managedObjectContext:NSManagedObjectContext, categoryId:double_t) -> Bool {
        do {
            return try Database.categories.deleteById(managedObjectContext, categoryId:categoryId)
        } catch {
            print(error as NSError)
        }
        return false
    }
    
    static func getCategories(managedObjectContext:NSManagedObjectContext) -> [CategoryObject]! {
        do {
            return try Database.categories.selectAll(managedObjectContext)
        } catch {
            print(error as NSError)
        }
        return nil
    }
    
    private static func checkCategoryExistsByCategoryName(managedObjectContext:NSManagedObjectContext, categoryName:String) -> Bool {
        do {
            if try Database.categories.exist(managedObjectContext, categoryName:categoryName) {
                print("// CategoryAPI: record exists")
                return true
            } else {
                print("// CategoryAPI: no record")
                return false
            }
        } catch {
            print(error as NSError)
        }
        return false
    }
    
    //    static func getCategoryIdByCategoryName(managedObjectContext:NSManagedObjectContext, categoryName:String) -> double_t {
    //        do {
    //            return try Database.categories.selectByName(managedObjectContext, categoryName:categoryName).thisId
    //        } catch {
    //            print(error as NSError)
    //        }
    //        return -1
    //    }
    //
    //    static func getCategoryByCategoryId(managedObjectContext:NSManagedObjectContext, id:double_t) -> CategoryObject! {
    //        do {
    //            return try Database.categories.selectById(managedObjectContext, categoryId:id)
    //        } catch {
    //            print(error as NSError)
    //            return nil
    //        }
    //    }
    //
    //    static func getIndexByCategoryId(managedObjectContext:NSManagedObjectContext, id:double_t) -> NSInteger! {
    //        var index:Int = 0
    //        for category in getCategories(managedObjectContext) {
    //            if (category.thisId == id) {
    //                return index
    //            }
    //            index += 1
    //        }
    //        return 0
    //    }
}
