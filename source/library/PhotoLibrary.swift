//
//  PhotoLibrary.swift
//  iLo
//
//  Created by wWheeler on 2015/12/23.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import Photos


class PhotoLibrary:NSObject {
    
    var images:NSMutableArray!
    
    var photosAsset: PHFetchResult!
    var assetCollection:PHAssetCollection = PHAssetCollection()
    
//    func fetchCollection() -> PHAssetCollection {
//        let result:PHFetchResult = fetchResult()
//        
//        
//        
//        //        return self.assetCollection
//        return result as! PHAssetCollection;
//    }
    
    func fetchResult() -> PHFetchResult {
        let requestOptions = PHImageRequestOptions()
        requestOptions.synchronous = true
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.Image.rawValue)
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        let result = PHAsset.fetchAssetsWithOptions(fetchOptions)
        
        //        let fetchOptions = PHFetchOptions()
        //        fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
        //        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"Date Created", ascending: true)]
        
        //        let result = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options:nil)
        //        let result = PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options:fetchOptions)
        
        if let first_Obj:AnyObject = result.firstObject{
            self.assetCollection = first_Obj as! PHAssetCollection
        }
        
        
        return self.assetCollection
    }
    
    
    
    func isPhotoLibraryAvilable() -> Bool {
        
        return true;
    }
    
    //    static func getSyncPhotos() {
    //
    ////        var albumFound:Bool = false
    ////        var photoAsset:PHFetchResult!
    //
    ////        var albumName = String("Test")
    //
    //        let fetchOptions = PHFetchOptions()
    ////        fetchOptions.predicate = NSPredicate(format: "title = %@", albumName)
    //        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
    //
    //        let assetCollection:PHAssetCollection!
    //        let collection:PHFetchResult = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options: fetchOptions)
    //
    //        if let first_Obj:AnyObject = collection.firstObject {
    //            //found the album
    //            assetCollection = collection.firstObject as! PHAssetCollection
    ////            albumFound = true
    //        }
    ////        else {
    ////            albumFound = false
    ////        }
    //
    //        return
    
    
    //        var i = collection.count
    //        photoAsset = PHAsset.fetchAssetsInAssetCollection(assetCollection, options: nil)
    //        let imageManager = PHCachingImageManager()
    //
    //        photoAsset.enumerateObjectsUsingBlock{(object: AnyObject!, count:Int, stop:UnsafeMutablePointer<ObjCBool>) in
    //
    //            if object is PHAsset{
    //                let asset = object as! PHAsset
    //                print("Inside  If object is PHAsset, This is number 1")
    //
    //                let imageSize = CGSize(width: asset.pixelWidth,
    //                    height: asset.pixelHeight)
    //
    //                /* For faster performance, and maybe degraded image */
    //                let options = PHImageRequestOptions()
    //                options.deliveryMode = .FastFormat
    //                options.synchronous = true
    //                imageManager.requestImageForAsset(asset,
    //                    targetSize:imageSize,
    //                    contentMode:.AspectFill,
    //                    options:options,
    //                    resultHandler: {
    //                        image, info in
    ////                        self.photo = image!
    //                        /* The image is now available to us */
    ////                        self.sendPhotos(self.photo)
    //                        print("enum for image, This is number 2")
    //
    //
    //                })
    //            }
    //        }
    //    }
}


//else{
//    // Album placeholder for the asset collection, used to reference collection in completion handler
//    var albumPlaceholder:PHObjectPlaceholder!
//    //create the folder
//    NSLog("\nFolder \"%@\" does not exist\nCreating now...", albumName)
//    PHPhotoLibrary.sharedPhotoLibrary().performChanges({
//        let request = PHAssetCollectionChangeRequest.creationRequestForAssetCollectionWithTitle(albumName)
//        albumPlaceholder = request.placeholderForCreatedAssetCollection
//        },
//        completionHandler: {(success:Bool, error:NSError?)in
//            if(success){
//                print("Successfully created folder")
//                self.albumFound = true
//                let collection = PHAssetCollection.fetchAssetCollectionsWithLocalIdentifiers([albumPlaceholder.localIdentifier], options: nil)
//                self.assetCollection = collection.firstObject as! PHAssetCollection
//            }else{
//                print("Error creating folder")
//                self.albumFound = false
//            }
//    })
//}
