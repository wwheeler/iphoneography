//
//  CameraHelper.swift
//  iLo
//
//  Created by Garrick Wheeler on 2015/12/30.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import Foundation
import MobileCoreServices
import UIKit

public class CameraHelper {
    
    static private var imagePickerController: UIImagePickerController?
    
    static func open(viewController:UIViewController, delegate:protocol<UIImagePickerControllerDelegate, UINavigationControllerDelegate>) {
        if !CameraHelper.isCameraAvailable() {
            CameraHelper.presentAlertViewController(viewController)
        } else {
            CameraHelper.openCamera(viewController, delegate:delegate)
        }
    }
    
    static func isCameraAvailable() -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.Camera) && cameraSupportsMedia(kUTTypeImage as String, sourceType: .Camera){
            return true
        } else {
            return false
        }
    }
    
    static func openCamera(viewController:UIViewController, delegate:protocol<UIImagePickerControllerDelegate, UINavigationControllerDelegate>) {
        imagePickerController = UIImagePickerController()
        if let theViewController = imagePickerController {
            theViewController.sourceType = .Camera
            theViewController.mediaTypes = [kUTTypeImage as String]
            theViewController.allowsEditing = false
            theViewController.delegate = delegate
            theViewController.cameraCaptureMode = UIImagePickerControllerCameraCaptureMode.Photo
            
            viewController.presentViewController(theViewController, animated: true, completion: nil)
        }
    }
    
    static func cameraSupportsMedia(mediaType: String, sourceType: UIImagePickerControllerSourceType) -> Bool {
        let availableMediaTypes = UIImagePickerController.availableMediaTypesForSourceType(sourceType) as [String]?
        if let types = availableMediaTypes {
            for type in types {
                if type == mediaType {
                    return true
                }
            }
        }
        return false
    }
    
    static func presentAlertViewController(viewController:UIViewController) {
        let alertController = UIAlertController(title:StringHelper.TitleCamera, message:StringHelper.MessageCamera, preferredStyle:.Alert)
        alertController.addAction(UIAlertAction(title:StringHelper.AlertDismiss, style:.Default, handler:nil))
        viewController.presentViewController(alertController, animated: true, completion: nil)
    }
}
