//
//  StaticValue.swift
//  iLo
//
//  Created by wWheeler on 2016/03/08.
//  Copyright © 2016 Company. All rights reserved.
//

class StringHelper {
    
    static var AlertOK = "Ok"
    static var AlertCancel = "Cancel"
    static var AlertDiscard = "Discard"
    static var AlertDismiss = "Dismiss"
    
    static var TitleCamera = "No Camera"
    static var MessageCamera = "You need a device with a camera to use this button"
    
    static var TitleDiscard = "Are you Sure?"
    static var MessageDiscard = "You have not saved this location yet"

    static var TitleCategory = "Create A Category"
    static var MessageCategory = "Please provide a category name"

    static var TitleInvalid = "Invalid Input"
    static var MessageInvalid = "Please enter a valid category name"
    
}