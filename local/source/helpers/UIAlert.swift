//
//  UIAlert.swift
//  iLo
//
//  Created by Garrick Michael Wheeler on 2016/01/16.
//  Copyright © 2016 Seagram Pearce. All rights reserved.
//

import Foundation
import UIKit

class UIAlert {
    static func show(viewController:UIViewController,
        title:String,
        message:String,
        negativeButtonText:String,
        negativeHandler:((UIAlertAction) -> Void)?,
        positiveButtonText:String,
        positiveHandler:((UIAlertAction) -> Void)?) {
            
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: negativeButtonText, style: UIAlertActionStyle.Default, handler: negativeHandler))
        viewController.presentViewController(alertController, animated: true, completion: nil)
        
        if(!positiveButtonText.isEmpty) {
            alertController.addAction(UIAlertAction(title: positiveButtonText, style: UIAlertActionStyle.Default, handler: positiveHandler))
        }
    }
    
    static func show(viewController:UIViewController, title:String, message:String) {
        show(viewController, title: title, message: message, negativeButtonText: "Okay", negativeHandler: nil, positiveButtonText: "", positiveHandler: nil)
    }
    
    static func show(viewController:UIViewController, title:String, message:String, negativeButtonText:String,
        negativeHandler:((UIAlertAction) -> Void)?) {
        show(viewController, title: title, message: message, negativeButtonText: negativeButtonText, negativeHandler: negativeHandler, positiveButtonText: "", positiveHandler: nil)
    }
    
//    static func show(viewController:UIViewController, title:String, message:String) {
//        ((UIAlertAction) -> Void)?
//    }
}
