//
//  StaticValue.swift
//  iLo
//
//  Created by wWheeler on 2016/03/08.
//  Copyright © 2016 Company. All rights reserved.
//

import UIKit

class StaticValuesHelper {
    
    
    //    Bools
    private static var readAndWriteBool:Bool!
    
    static func setReadOnly() { self.readAndWriteBool = false }
    static func setReadAndWrite() { self.readAndWriteBool = true }
    
    static func isReadAndWrite() -> Bool {
        if (self.readAndWriteBool == nil) {
            self.readAndWriteBool = true
        }
        return self.readAndWriteBool
    }
    
    //    Objects
    private static var mCategoryObj:CategoryObject?
    private static var mLocationObj:LocationObject?
    private static var mImageObj:ImageObject?
    private static var mImage:UIImage?
    
    //    setters
    static func setCategoryObj(category:CategoryObject!) { self.mCategoryObj = category }
    static func setLocationObj(location:LocationObject) { self.mLocationObj = location }
    static func setImageObj(image:ImageObject) { self.mImageObj = image }
    static func setImage(image:UIImage) { self.mImage = image }
    
    //    getters
    static func getCategoryObj() -> CategoryObject? { return self.mCategoryObj }
    static func getLocationObj() -> LocationObject? { return self.mLocationObj }
    static func getImageObj() -> ImageObject? { return self.mImageObj }
    static func getImage() -> UIImage? { return self.mImage }
    
    //    resetters
    static func resetCategoryObj() { self.mCategoryObj = nil }
    static func resetLocationObj() { self.mLocationObj = nil }
    static func resetImageObj() { self.mImageObj = nil }
    static func resetImage() { self.mImage = nil }
    
    static func resetAllObj() {
        self.resetCategoryObj()
        self.resetLocationObj()
        self.resetImageObj()
    }
    
    static func resetAll() {
        self.resetCategoryObj()
        self.resetLocationObj()
        self.resetImageObj()
        self.resetImage()
    }
    
    
//    private static var isCanceledCameraBoo:Bool = false
//    static func isCanceledCamera() -> Bool! {
//        if(!self.isCanceledCameraBoo) {
//            self.isCanceledCameraBoo = true
//            return false
//        }
//        return true
//    }
//
    //    static func resetCanceledCamera() { self.isCanceledCameraBoo = false }
    //        self.resetCanceledCamera()
    

}