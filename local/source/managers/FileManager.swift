//
//  FileManager.swift
//  iLo
//
//  Created by wWheeler on 2015/12/30.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import Foundation


class FileManager:NSObject {
    
    var isLogging:Bool = true
    var isFullPath:Bool = false
    var error:NSError? = nil
    
    var fileManager:NSFileManager = NSFileManager.defaultManager()
    
    static func getInstance() -> FileManager {
        let sharedInstance = FileManager()
        return sharedInstance
    }
    
    func createDirectoryAtPath(path:String) {
        if !isDirectoryAtPath(path) {
            do {
                try fileManager.createDirectoryAtPath(path, withIntermediateDirectories:true, attributes:nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
    }
    
    func removeFileAtPath(path:String) {
        if isFileAtPath(path) {
            do {
                try fileManager.removeItemAtPath(path)
            } catch {
                print("Error removing file at path:", path)
            }
        }
    }
    
    func isFileAtPath(path:String) -> Bool {
        var isDirectory:ObjCBool = false
        if fileManager.fileExistsAtPath(path, isDirectory:&isDirectory) {
            return true
        }
        return false
    }
    
    func isDirectoryAtPath(path:String) -> Bool {
        var isDirectory:ObjCBool = false
        if fileManager.fileExistsAtPath(path, isDirectory:&isDirectory) {
            if isDirectory {
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    // Documents Folder Functions
    
    func documentsEnumerator() -> NSDirectoryEnumerator {
        let files = fileManager.enumeratorAtPath(documents())
        return files!
    }
    
    func documentsEnumeratorAtPath(path:String) -> NSDirectoryEnumerator {
        let files = fileManager.enumeratorAtPath(documentsWithPath(path))
        return files!
    }
    
    func documentsWithPath(path:String) -> String {
        let files = documentsURLs().URLByAppendingPathComponent(path)
        return files.path!
    }
    
    func documentsURLs() -> NSURL {
        let documentsURL = fileManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func documentsURLsWithPath(path:String) -> NSURL {
        let files = documentsURLs().URLByAppendingPathComponent(path)
        return files
    }
    
    func documents() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let directory: AnyObject = paths[0]
        return directory as! String
    }
    
    func printDocuments() {
        if (isLogging) {
            print("\n")
            print("~/Documents:")
            for filePath in documentsEnumerator().allObjects {
                print(filePath)
            }
        }
    }
}