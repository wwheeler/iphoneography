//
//  TableViewController.swift
//  iLo
//
//  Created by wWheeler on 2016/01/18.
//  Copyright © 2016 Company. All rights reserved.
//

import UIKit

let libraryTableViewCell = "libraryTableViewCell"
let libraryCollectionViewCell = "libraryCollectionViewCell"

class LibraryViewController:UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    var categories:[CategoryObject]!
    var locations:[LocationObject]!
    
    var managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    // MARK:- API Methods
    
    func saveCategory(name:String) {
        CategoryAPI.save(self.managedContext, categoryName:name)
    }
    
    func deleteCategory(id:double_t) -> Bool! {
        return CategoryAPI.deleteByCategoryId(managedContext, categoryId:id)
    }
    
    func getCategories() -> [CategoryObject]! {
        return CategoryAPI.getCategories(managedContext)
    }
    
    func getLocationsByCategoryId(id:double_t) -> [LocationObject]! {
        return LocationAPI.getLocationsByCategoryId(managedContext, id:id)
    }
    
    func getFirstImagePathsByLocationNamesWithCategoryId(id:double_t) -> [String] {
        return ImageAPI.getFirstImagePathsByLocationNamesWithCategoryId(managedContext, id:id)
    }
    
    func getImagesPathsByLocationName(locationName:String) -> [String] {
        return ImageAPI.getImagesPathsByLocationName(managedContext, locationName:locationName)
    }
    
    // MARK:- LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clearColor()
        
        self.addButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 152
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(libraryTableViewCell, forIndexPath: indexPath) as! LibraryTableViewCell
        let category = categories[indexPath.row]
        
        cell.label.text = category.name.uppercaseString + "\n#00" + String(indexPath.row + 1)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        guard let tableViewCell = cell as? LibraryTableViewCell else { return }
        
        let locations = self.getLocationsByCategoryId(categories[indexPath.row].thisId)
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow:(locations.count > 0) ? indexPath.row : -1)
    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        guard let _ = cell as? LibraryTableViewCell else { return }
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // TODO deselection doesnt work properly after cancelling delete
            let category = categories[indexPath.row]
            if self.deleteCategory(category.thisId) == true {
                self.reloadData()
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        StaticValuesHelper.setCategoryObj(categories[indexPath.row])
        let locations = self.getLocationsByCategoryId(categories[indexPath.row].thisId)
        
        if locations.count > 0 {
            StaticValuesHelper.setReadOnly()
            NavigationHelper.presentWithBack("locationsTableViewController", parentView: self);
        } else {
            StaticValuesHelper.setReadAndWrite()
            NavigationHelper.presentWithBack("locationViewController", parentView: self);
        }
    }
    
    // MARK:- Data Methods
    
    func reloadData() {
        self.categories = self.getCategories()
        self.tableView.reloadData()
    }
    
    // MARK:- Other
    
    func handleButtonActions(sender:UIButton!) {
        if (sender == self.addButton){
            self.createCategory()
        }
    }
    
    func createCategory() {
        let alertController = UIAlertController(title:StringHelper.TitleCategory, message:StringHelper.MessageCategory, preferredStyle: .Alert)
        alertController.addTextFieldWithConfigurationHandler(nil)
        
        let okAction = UIAlertAction(title:StringHelper.AlertOK, style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            let textField = alertController.textFields![0] as UITextField
            if (textField.text!.isEmpty == true) {
                UIAlert.show(self, title:StringHelper.TitleInvalid, message:StringHelper.MessageInvalid, negativeButtonText:StringHelper.AlertOK, negativeHandler: {
                    (okAction) -> Void in
                    self.createCategory()
                })
            } else {
                self.saveCategory(textField.text!)
                self.reloadData()
            }
        }
        
        let cancelAction = UIAlertAction(title:StringHelper.AlertCancel, style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}

// MARK:- Extension Methods

extension LibraryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == -1 {
            return 1
        } else {
            let category = categories[collectionView.tag]
            let locations = self.getLocationsByCategoryId(category.thisId)
            return locations.count
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(libraryCollectionViewCell, forIndexPath: indexPath) as! LibraryCollectionViewCell
        cell.backgroundColor = UIColor.init(colorLiteralRed: 65/255, green: 65/255, blue: 65/255, alpha: 1)
        
        if collectionView.tag == -1 {
            cell.imageView.image = nil
        } else {
            let category = categories[collectionView.tag]
            let imagePaths = self.getFirstImagePathsByLocationNamesWithCategoryId(category.thisId)
            
            if (imagePaths[indexPath.row] == "") {
                cell.addImageView.hidden = true
                cell.imageView.image = nil
                cell.backgroundColor = UIColor.randomColor()
            } else {
                cell.addImageView.hidden = false
                cell.imageView.contentMode = .ScaleAspectFill
                cell.imageView.image = UIImage(named:imagePaths[indexPath.row])
                cell.backgroundColor = UIColor.clearColor()
            }
        }
        
        return cell
    }
}

extension UIColor {
    class func randomColor() -> UIColor {
        let hue = CGFloat(arc4random() % 100) / 100
        let saturation = CGFloat(arc4random() % 100) / 100
        let brightness = CGFloat(arc4random() % 100) / 100
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
}
