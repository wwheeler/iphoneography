//
//  LibraryCollectionViewCell.swift
//  iLo
//
//  Created by wwheeler on 2016/04/24.
//  Copyright © 2016 Company. All rights reserved.
//

import UIKit

public class LibraryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var addImageView: UIImageView!
}