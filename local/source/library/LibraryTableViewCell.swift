//
//  LibraryCell
//  iLo
//
//  Created by wWheeler on 2015/12/22.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import UIKit

public class LibraryTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var label: UILabel!    
}

extension LibraryTableViewCell {
    func setCollectionViewDataSourceDelegate<D: protocol<UICollectionViewDataSource, UICollectionViewDelegate>>(dataSourceDelegate: D, forRow row: Int) {
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.reloadData()
    }
}