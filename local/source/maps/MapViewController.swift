//
//  MapViewController.swift
//  iLo
//
//  Created by wWheeler on 2015/12/15.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: ParentViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    var locationManager:CLLocationManager!
    var currentlocation:CLLocation!
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    
    // MARK:-
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        updateLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Location
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentlocation = locations.first
            
            print("Found user's location: \(location)")
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
            
            self.mapView.setRegion(region, animated: true)
            self.locationManager.stopUpdatingLocation()
            //self.locationManager.saveLocation(location)
        }
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.NotDetermined {
            self.locationManager.requestAlwaysAuthorization()
        } else if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            self.locationManager.startUpdatingLocation()
        } else if status == CLAuthorizationStatus.AuthorizedAlways {
            self.locationManager.startUpdatingLocation()
        } else if status == CLAuthorizationStatus.Restricted {
            print("restricted by e.g. parental controls. User can't enable Location Services")
        } else if status == CLAuthorizationStatus.Denied {
            print("user denied your app access to Location Services, but can grant access from Settings.app")
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    // MARK:- Private
    private func setup() {
        self.backButton.addTarget(self, action:#selector(MapViewController.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.locationButton.addTarget(self, action:#selector(MapViewController.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        //Getting touch Gesture to add bookmark
        let uilpgr = UILongPressGestureRecognizer(target: self, action: #selector(MapViewController.handleAction(_:)))
        uilpgr.minimumPressDuration = 0.1
        uilpgr.numberOfTouchesRequired = 1
        
        self.mapView.delegate = self
        self.mapView.showsUserLocation = true
        self.mapView.addGestureRecognizer(uilpgr)
    }
    
    private func updateLocation() {
        if (self.locationManager == nil) {
            self.locationManager = CLLocationManager()
        }
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func handleButtonActions(sender:UIButton!) {
        if sender == backButton {
            NavigationHelper.dismiss(self)
        } else if sender == locationButton {
            NavigationHelper.dismiss(self)
            //            NavigationHelper.presentWithBack("locationViewController", parentView: self);            
        }
    }
    
    func handleAction(gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizerState.Began {
            let annotation = MKPointAnnotation()
            annotation.coordinate = self.mapView.convertPoint(gestureRecognizer.locationInView(self.mapView), toCoordinateFromView:self.mapView)
            annotation.title =  "Untitled"
            self.mapView.addAnnotation(annotation)
        } else if gestureRecognizer.state == UIGestureRecognizerState.Changed {
            if let annotation = (self.mapView.annotations.filter{$0.title! == "Untitled"}).first as? MKPointAnnotation {
                annotation.coordinate = self.mapView.convertPoint(gestureRecognizer.locationInView(self.mapView), toCoordinateFromView:self.mapView)
            }
        } else if gestureRecognizer.state == UIGestureRecognizerState.Ended {
            if let annotation = (mapView.annotations.filter{$0.title! == "Untitled" }).first as? MKPointAnnotation {
                let alert = UIAlertController(title: "New Annotation", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                var inputTextField: UITextField?
                alert.addAction(UIAlertAction(title: "Add", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                    if  let annotationTitle = inputTextField?.text {
                        annotation.title =  annotationTitle
                        annotation.subtitle = "Lat:\(String(format: "%.06f", annotation.coordinate.latitude))  Lon:\(String(format: "%.06f", annotation.coordinate.longitude))"
                    }
                }))
                alert.addTextFieldWithConfigurationHandler({ textField in
                    textField.placeholder = "Place Description"
                    inputTextField = textField
                })
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                    self.mapView.removeAnnotation(annotation)
                }))
                presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    /*
    private func showAlert() {
    // create the alert
    let alert = UIAlertController(title: "UIAlertController", message: "Would you like to continue learning how to use iOS alerts?", preferredStyle: UIAlertControllerStyle.Alert)
    
    // add the actions (buttons)
    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
    alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler:{
    action in
    self.locationManager.startUpdatingLocation()
    }))
    
    // show the alert
    self.presentViewController(alert, animated: true, completion: nil)
    }
    */
}