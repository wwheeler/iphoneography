//
//  ImageModel.swift
//  iLo
//
//  Created by wWheeler on 2016/03/08.
//  Copyright © 2016 Company. All rights reserved.
//

import UIKit
import Foundation

class ImageModel {
    
    var name:String = ""
    var locationName:String = ""
    
    var imageObj:ImageObject!
    var imagesObj:[ImageObject]!
    var paths:[String]!
    var image:UIImage!
    
    func initWithObject(object:ImageObject) {
        self.name = object.name
        self.locationName = object.locationName
        
        self.imageObj = object
    }
    
    func initWithObjects(objects:[ImageObject]) {
        self.imagesObj = objects
    }
}