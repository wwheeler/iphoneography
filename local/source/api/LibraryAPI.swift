//
//  LibraryAPI.swift
//  iLo
//
//  Created by wWheeler on 2015/12/29.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import UIKit
import CoreData

class LibraryAPI:NSObject {
    
    static var filename:String?
    static var libraryPath:String?
    static var libraryFolder:String = "ilo/library"
    
    static var fileManager:FileManager = FileManager()
    
    static func createLibraryFolder() {
        self.libraryPath = fileManager.documentsWithPath(self.libraryFolder)
        if self.isLibraryFolderAvailable() == false {
            fileManager.createDirectoryAtPath(libraryPath!)
        }
    }
    
    static func saveImage(image:UIImage) -> String {
        let UUID = String(NSDate().timeIntervalSince1970)
        self.filename = UUID + ".png"
        
        let fileURL:String = self.libraryPath! + "/" + self.filename!
        if saveImageToLibrary(image, path:fileURL) {
            return self.filename!
        }
        return self.filename!
    }
    
    // MARK:- Private Methods
    
    private static func isLibraryFolderAvailable() -> Bool {
        if fileManager.isDirectoryAtPath(self.libraryPath!) == true {
            return true
        }
        return false
    }
    
    private static func saveImageToLibrary(image: UIImage, path: String ) -> Bool {
        let pngImageData = UIImagePNGRepresentation(image)
        let result = pngImageData!.writeToFile(path, atomically: true)
        if (result) {
            return true
        }
        return false
    }
}