//
//  DataAPI.swift
//  iLo
//
//  Created by wWheeler on 2016/03/09.
//  Copyright © 2016 Company. All rights reserved.
//

import CoreData

class LocationAPI {
    
    static func save(managedObjectContext:NSManagedObjectContext, locationModel:LocationModel) {
        do {
            let locationObj = try Database.locations.exist(managedObjectContext, categoryId:0.0, locationName:locationModel.name)
            if (locationObj != nil) {
                if (locationObj?.categoryId == locationModel.categoryId &&
                    locationObj?.name == locationModel.name) {
                    self.update(managedObjectContext, locationModel:locationModel)
                } else {
                    self.insert(managedObjectContext, locationModel:locationModel)
                }
            } else {
                self.insert(managedObjectContext, locationModel:locationModel)
            }
        } catch {
            print(error as NSError)
        }
    }
    
    static func insert(managedObjectContext:NSManagedObjectContext, locationModel:LocationModel) {
        Database.locations.insert(managedObjectContext,
                                  categoryId:locationModel.categoryId,
                                  name:locationModel.name,
                                  location:locationModel.location,
                                  date:locationModel.date,
                                  comment:locationModel.comment)
    }
    
    static func update(managedObjectContext:NSManagedObjectContext, locationModel:LocationModel) {
        Database.locations.update(managedObjectContext,
                                  categoryId:locationModel.categoryId,
                                  name:locationModel.name,
                                  location:locationModel.location,
                                  date:locationModel.date,
                                  comment:locationModel.comment)
    }
    
    static func deleteByLocationName(managedObjectContext:NSManagedObjectContext, locationName:String) -> Bool {
        do {
            return try Database.locations.deleteByLocationName(managedObjectContext, locationName:locationName)
        } catch {
            print(error as NSError)
        }
        return false
    }
    
    static func getLocationsByCategoryId(managedObjectContext:NSManagedObjectContext, id:double_t) -> [LocationObject]! {
        do {
            return try Database.locations.selectByCategoryId(managedObjectContext, categoryId:id)
        } catch {
            print(error as NSError)
        }
        return nil
    }
    
    static func getLocationByLocationName(managedObjectContext:NSManagedObjectContext, locationName:String) -> LocationObject! {
        do {
            return try Database.locations.selectByLocationName(managedObjectContext, locationName: locationName)
        } catch {
            print(error as NSError)
        }
        return nil
    }
}
