//
//  CategoryObject.swift
//  iLo
//
//  Created by Garrick Michael Wheeler on 2016/01/12.
//  Copyright © 2016 Seagram Pearce. All rights reserved.
//

import Foundation
import CoreData

@objc(CategoryObject)
public class CategoryObject:NSManagedObject {
    @NSManaged var thisId:Double
    @NSManaged var name:String
}
