//
//  LocationObject.swift
//  iLo
//
//  Created by Garrick Michael Wheeler on 2016/01/12.
//  Copyright © 2016 Seagram Pearce. All rights reserved.
//

import Foundation
import CoreData

@objc(LocationObject)
public class LocationObject:NSManagedObject {
    @NSManaged var uniqueId:Double
    @NSManaged var categoryId:Double
    @NSManaged var name:String
    @NSManaged var location:String
    @NSManaged var date:NSDate
    @NSManaged var comment:String
}
