//
//  CategoryTableViewController.swift
//  iLo
//
//  Created by wWheeler on 2016/03/08.
//  Copyright © 2016 Company. All rights reserved.
//

import UIKit

let categoryTableViewCell = "categoryTableViewCell"

class CategoryTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var categories:[CategoryObject]!
    //    var categoryId:double_t!
    //    var row:Int = 0
    
    var managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    // MARK:- CategoryAPI
    
    func saveCategory(name:String) {
        CategoryAPI.save(managedContext, categoryName:name)
    }
    
    func deleteCategory(id:double_t) -> Bool! {
        return CategoryAPI.deleteByCategoryId(managedContext, categoryId:id)
    }
    
    func getCategories() -> [CategoryObject]! {
        return CategoryAPI.getCategories(managedContext)
    }
    
    //    func getIndexByCategoryId(id:double_t) -> NSInteger! {
    //        return CategoryAPI.getIndexByCategoryId(managedContext, id:id)
    //    }
    
    // MARK:- Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        self.addButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.backButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.categories = self.getCategories()
        //        if (StaticValuesHelper.getCategoryObj() != nil) {
        //            self.row = self.getIndexByCategoryId(StaticValuesHelper.getCategoryObj()!.thisId)
        //        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 46
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(categoryTableViewCell, forIndexPath: indexPath) as! CategoryTableViewCell
        //        if (indexPath.row == row) {
        //            cell.accessoryType = .Checkmark
        //            cell.selectionStyle = .None
        //            tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.Top)
        //        } else {
        //            cell.accessoryType = .None
        //            cell.selectionStyle = .None
        //        }
        
        cell.backgroundColor = UIColor.clearColor()
        cell.label.text = categories[indexPath.row].name
        
        tableView.separatorColor = UIColor.whiteColor().colorWithAlphaComponent(0.1)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            cell.selectionStyle = .None
            cell.accessoryType = .None
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            cell.selectionStyle = .None
            cell.accessoryType = .Checkmark
            
            StaticValuesHelper.setCategoryObj(categories[indexPath.row])
            NavigationHelper.dismiss(self)
        }
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let category = categories[indexPath.row]
            if self.deleteCategory(category.thisId) == true {
                self.categories = self.getCategories()
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK:- Data Methods
    
    //    func dismissView() {
    //        self.categories = self.getCategories()
    //        self.row = self.getIndexByCategoryId(self.categoryId)
    //        StaticValuesHelper.setCategoryObj(self.categories[self.row])
    //        NavigationHelper.dismiss(self)
    //    }
    
    // MARK:- Other
    
    func handleButtonActions(sender:UIButton!) {
        if sender == backButton {
            NavigationHelper.dismiss(self)
        } else if sender == addButton {
            self.createCategory()
        }
    }
    
    func createCategory() {
        let alertController = UIAlertController(title:StringHelper.TitleCategory, message:StringHelper.MessageCategory, preferredStyle: .Alert)
        alertController.addTextFieldWithConfigurationHandler(nil)
        
        let okAction = UIAlertAction(title:StringHelper.AlertOK, style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            let textField = alertController.textFields![0] as UITextField
            if (textField.text!.isEmpty == true) {
                UIAlert.show(self, title:StringHelper.TitleInvalid, message:StringHelper.MessageInvalid, negativeButtonText:StringHelper.AlertOK, negativeHandler: {
                    (okAction) -> Void in
                    self.createCategory()
                })
            } else {
                //                self.categoryId =
                self.saveCategory(textField.text!)
                NavigationHelper.dismiss(self)
            }
        }
        
        let cancelAction = UIAlertAction(title:StringHelper.AlertCancel, style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}