//
//  LocationViewController.swift
//  iLo
//
//  Created by Garrick Wheeler on 2015/12/23.
//  Copyright © 2015 Seagram Pearce. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices

class LocationViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var moreImagesScrollView: UIScrollView!
    
    @IBOutlet var mainImageView: UIImageView!
    @IBOutlet var datePicker: UIDatePicker!
    
    @IBOutlet var locationNameInput: UITextField!
    @IBOutlet var commentsInput: UITextView!
    @IBOutlet var locationTextView: UITextView!
    
    @IBOutlet var locationTextLabel: UILabel!
    @IBOutlet var dateTextLabel: UILabel!
    
    @IBOutlet var dateTextView: UIView!
    @IBOutlet var datePickerView: UIView!
    
    @IBOutlet var addImagesButton: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var categoryButton: UIButton!
    @IBOutlet var datePickerDoneButton: UIButton!
    @IBOutlet var datePickerCancelButton: UIButton!
    @IBOutlet var locationButton: UIButton!
    
    private final var THUMB_SIZE_FINAL = 75
    
    var keyboardHeight: CGFloat = 0.0
    var duration:NSTimeInterval = 0.0
    var animationCurve:UIViewAnimationOptions = [];
    var imageButtons:[UIButton] = []
    var blurView:UIVisualEffectView?
    
    var categoryModel:CategoryModel = CategoryModel()
    var locationModel:LocationModel = LocationModel()
    var imageModel:ImageModel = ImageModel()
    
    var managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    // MARK:- API Methods
    
    func getImagesPathsByLocationName(name:String) -> [String] {
        return ImageAPI.getImagesPathsByLocationName(managedContext, locationName: name)
    }
    
    func getImagesByLocationName(name:String) -> [ImageObject] {
        return ImageAPI.getImagesByLocationName(managedContext, locationName: name)
    }
    
    // MARK:- LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup Interface
        blurView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
        blurView!.alpha = 0.5
        blurView!.frame = CGRectMake(0, 0, CGFloat(THUMB_SIZE_FINAL), CGFloat(THUMB_SIZE_FINAL))
        datePickerView.hidden = false
        datePickerView.alpha = 0.0
        
        // add the listeners
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard)))
        
        backButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        addImagesButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        datePickerDoneButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        datePickerCancelButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        locationButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        categoryButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        locationTextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.setLocationPicker)))
        dateTextView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.openDatePicker)))
        mainImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.changeMainImageScale)))
        
        // delegates
        self.locationNameInput.delegate = self
        self.commentsInput.delegate = self        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadData()
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: AnyObject]){
        dispatch_async(dispatch_get_main_queue(), {
            picker.dismissViewControllerAnimated(false, completion: nil)
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                StaticValuesHelper.setImage(pickedImage.orientationCorrection())
            }
        })
    }
    
    // MARK:- Data Methods
    
    func canSaveData() -> Bool {
        if locationModel.categoryId > -1 {
            if locationModel.name != "" {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func saveData() {
        imageModel.name = LibraryAPI.saveImage(StaticValuesHelper.getImage()!)
        imageModel.locationName = locationModel.name
        
        ImageAPI.save(managedContext, imageModel:imageModel)
        LocationAPI.save(managedContext, locationModel:locationModel)
    }
    
    func reloadData() {
        if (StaticValuesHelper.getImage() != imageModel.image) {
            imageModel.image = StaticValuesHelper.getImage()
            self.setMainImage(imageModel.image)
            
            self.setDate(NSDate())
        }
        
        if (StaticValuesHelper.getCategoryObj() != categoryModel.categoryObj) {
            categoryModel.initWithObject(StaticValuesHelper.getCategoryObj()!)
            self.setCategory(categoryModel.name)
            
            locationModel.categoryId = categoryModel.thisId
        }
        
        if (StaticValuesHelper.getLocationObj() != locationModel.locationObj) {
            locationModel.initWithObject(StaticValuesHelper.getLocationObj()!)
            
            self.setLocationName(locationModel.name)
            self.setLocation(locationModel.location)
            self.setDate(locationModel.date)
            self.setComment(locationModel.comment)
            
            imageModel.initWithObjects(self.getImagesByLocationName(locationModel.name))
            
            if imageModel.imagesObj.count > 0 {
                imageModel.paths = self.getImagesPathsByLocationName(locationModel.name)
                self.mainImageView.image = UIImage(named:imageModel.paths[0])
                
                self.buildScrollView()
                self.disableButtons()
            }
        }
    }
    
    
    // MARK:- Button Methods
    
    func handleButtonActions(sender:UIButton!) {
        if sender == addImagesButton {
            CameraHelper.openCamera(self, delegate: self)
        } else if sender == datePickerCancelButton {
            hideDatePicker()
        } else if sender == datePickerDoneButton {
            hideDatePicker()
            setDate(datePicker.date)
        } else if sender == backButton {
            if (StaticValuesHelper.isReadAndWrite()) {
                if (self.canSaveData()) {
                    self.saveData()
                    NavigationHelper.dismiss(self)
                } else {
                    let alertController = UIAlertController(title:StringHelper.TitleDiscard, message:StringHelper.MessageDiscard, preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title:StringHelper.AlertDiscard, style: UIAlertActionStyle.Default, handler: {
                        (action: UIAlertAction!) in
                        NavigationHelper.dismiss(self)
                    }))
                    alertController.addAction(UIAlertAction(title:StringHelper.AlertCancel, style: UIAlertActionStyle.Default,handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            } else {
                NavigationHelper.dismiss(self)
            }
        } else if sender == categoryButton {
            NavigationHelper.presentWithBack("categoryTableViewController", parentView: self);
        }
    }
    
    // MARK:- Set Methods
    
    func setCategory(title:String) {
        categoryButton.titleLabel!.font = UIFont(descriptor:(categoryButton.titleLabel?.font.fontDescriptor())!, size: 20)
        categoryButton.titleLabel?.textAlignment = .Center
        categoryButton.setTitle(title.uppercaseString, forState: .Normal)
    }
    
    func setLocationName(title:String) {
        locationNameInput.font = UIFont(descriptor:(dateTextLabel.font.fontDescriptor()), size: 14)
        locationNameInput.textAlignment = .Center
        locationNameInput.text = title.uppercaseString
    }
    
    func setDate(input:NSDate) {
        locationModel.date = input

        let formatter = NSDateFormatter()
        formatter.dateStyle = .LongStyle
        
        dateTextLabel.font = UIFont(descriptor:(dateTextLabel.font.fontDescriptor()), size: 14)
        dateTextLabel.textAlignment = .Center
        dateTextLabel.text = formatter.stringFromDate(input).uppercaseString
    }
    
    func setComment(input:String) {
        commentsInput.font = UIFont(descriptor:(dateTextLabel.font.fontDescriptor()), size: 14)
        commentsInput.textAlignment = .Center
        commentsInput.text = input.uppercaseString
    }
    
    func setLocation(input:String) {
        locationTextLabel.font = UIFont(descriptor:(dateTextLabel.font.fontDescriptor()), size: 14)
        locationTextLabel.textAlignment = .Center
        locationTextLabel.text = input.uppercaseString
    }
    
    func setLocationPicker() {
        print("setLocation")
        NavigationHelper.presentWithBack("mapViewController", parentView: self);
    }
    
    func setMainImage(image:UIImage) {
        self.mainImageView.contentMode = .ScaleAspectFill
        self.mainImageView.image = image
    }
    
    // MARK:- Image Methods
    
    func handleImageActions(sender:UIButton!) {
        let btnsendtag:UIButton = sender
        
        UIView.transitionWithView(self.mainImageView,
                                  duration:0.5,
                                  options: UIViewAnimationOptions.TransitionCrossDissolve,
                                  animations: { self.setMainImage(UIImage(contentsOfFile:self.imageModel.paths[btnsendtag.tag])!) },
                                  completion: nil)
        
        // clear the blurs
        blurView!.removeFromSuperview()
        
        // add the new one
        sender.addSubview(blurView!)
        
        blurView!.alpha = 0
        UIView.animateWithDuration(0.5) {
            self.blurView!.alpha = 0.4
        }
    }
    
    func buildScrollView() {
        imageButtons.removeAll()
        for subview in moreImagesScrollView.subviews {
            subview.removeFromSuperview()
        }
        
        let placeHolder = UIView(frame: CGRectMake(0, 0, CGFloat(THUMB_SIZE_FINAL * imageModel.paths.count), CGFloat(THUMB_SIZE_FINAL)))
        for index in 0 ..< imageModel.paths.count {
            let newImage = UIButton (frame: CGRectMake(0 + CGFloat(index * THUMB_SIZE_FINAL), 0, CGFloat(THUMB_SIZE_FINAL), CGFloat(THUMB_SIZE_FINAL)))
            newImage.tag = index
            newImage.contentMode = .ScaleAspectFill
            newImage.setImage(UIImage(named: imageModel.paths[index]), forState: UIControlState.Normal)
            newImage.addTarget(self, action: #selector(self.handleImageActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            imageButtons.append(newImage)
            placeHolder.addSubview(newImage)
        }
        
        moreImagesScrollView.addSubview(placeHolder)
        moreImagesScrollView.contentSize = CGSize.init(width: CGFloat(THUMB_SIZE_FINAL * imageModel.paths.count), height: CGFloat(THUMB_SIZE_FINAL))
    }
    
    func fixLayoutFromUIAlertController() {
        self.view.endEditing(true)
        UIView.animateWithDuration(0,
                                   delay: NSTimeInterval(0),
                                   options: animationCurve,
                                   animations: { self.view.layoutIfNeeded() },
                                   completion: nil)
    }
    
    func changeMainImageScale() {
        // feature to be added later
        //        UIView.animateWithDuration(0.5) {
        //            if self.mainImageView.contentMode == .ScaleAspectFill {
        //                self.mainImageView.contentMode = .ScaleAspectFit
        //            } else {
        //                self.mainImageView.contentMode = .ScaleAspectFill
        //            }
        //        }
    }
    
    // MARK:- DatePicker Methods
    
    func openDatePicker() {
        dismissKeyboard()
        UIView.animateWithDuration(0.3) {
            self.datePickerView!.alpha = 1.0
        }
    }
    
    func hideDatePicker() {
        UIView.animateWithDuration(0.3) {
            self.datePickerView!.alpha = 0.0
        }
    }
    
    // MARK:- TextField Methods
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.scrollView.setContentOffset(CGPointMake(0, 250), animated: true)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if (textField == locationNameInput){
            locationModel.name = textField.text!
        }
        self.scrollView.setContentOffset(CGPointMake(0, 0), animated: true)
    }
    
    // MARK:- TextView Methods
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        self.scrollView.setContentOffset(CGPointMake(0, 0), animated: true)
        return true;
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        self.scrollView.setContentOffset(CGPointMake(0, 350), animated: true)
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if (textView == commentsInput) {
            locationModel.comment = textView.text!
        }
        self.scrollView.setContentOffset(CGPointMake(0, 0), animated: true)
    }
    
    // MARK:- Keyboard Methods
    
    func dismissKeyboard() {
        view.endEditing(true)
        self.scrollView.setContentOffset(CGPointMake(0, 0), animated: true)
    }
    
    // MARK:- Other Methods
    
    func disableButtons() {
        locationNameInput.enabled = false
        datePicker.enabled = false
        addImagesButton.enabled = false
        categoryButton.enabled = false
        locationButton.enabled = false
        
        commentsInput.userInteractionEnabled = false
        locationTextView.userInteractionEnabled = false
        
        dateTextView.userInteractionEnabled = false
        datePickerView.userInteractionEnabled = false
    }
}
