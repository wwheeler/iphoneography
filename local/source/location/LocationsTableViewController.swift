//
//  LocationsTableViewController.swift
//  iLo
//
//  Created by wWheeler on 2016/03/08.
//  Copyright © 2016 Company. All rights reserved.
//

import UIKit

let locationsTableViewCell = "locationsTableViewCell"

class LocationsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var category:CategoryObject!
    var locations:[LocationObject]!
    var categoryId:double_t!
    //    var row:Int = 0
    
    var managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    // MARK:- API Methods
    
    func saveLocation(name:String) {
        let locationModel:LocationModel = LocationModel()
        locationModel.categoryId = self.categoryId
        locationModel.name = name
        LocationAPI.save(managedContext, locationModel:locationModel)
    }
    
    func deleteLocation(name:String) -> Bool {
        return LocationAPI.deleteByLocationName(managedContext, locationName:name)
    }
    
    func getFirstImagePathByLocationName(name:String) -> String {
        return ImageAPI.getFirstImagePathByLocationName(managedContext, locationName:name)
    }
    
    func getImagePathsByLocationName(name:String) -> [String] {
        return ImageAPI.getImagesPathsByLocationName(managedContext, locationName:name)
    }
    
    func getLocationsByCategoryId(id:double_t) -> [LocationObject]! {
        return LocationAPI.getLocationsByCategoryId(managedContext, id:id)
    }
    
    // MARK:- Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clearColor()
        
        self.backButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.addButton.addTarget(self, action: #selector(self.handleButtonActions(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        self.loadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locations.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(locationsTableViewCell, forIndexPath: indexPath) as! LocationsTableViewCell
        
        let locationName = self.locations[indexPath.row].name
        let imagePath = self.getFirstImagePathByLocationName(locationName)
        
        cell.label.text = locationName
        
        if (imagePath == "") {
            cell.imgView.image = nil
            cell.backgroundColor = UIColor.randomColor()
        } else {
            cell.imgView.contentMode = .ScaleAspectFill
            cell.imgView.image = UIImage(named:imagePath)
            cell.backgroundColor = UIColor.clearColor()
        }
        
        tableView.separatorColor = UIColor.whiteColor().colorWithAlphaComponent(0.1)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        StaticValuesHelper.setLocationObj(locations[indexPath.row])
        NavigationHelper.presentWithBack("locationViewController", parentView: self);
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let location = locations[indexPath.row]
            if self.deleteLocation(location.name) == true {
                self.loadData()
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK:- Data Methods
    
    func loadData() {
        self.category = StaticValuesHelper.getCategoryObj()
        self.categoryId = self.category.thisId
        self.locations = self.getLocationsByCategoryId(self.categoryId)
    }
    
    // MARK:- Other
    
    func handleButtonActions(sender:UIButton!) {
        if sender == backButton {
            NavigationHelper.dismiss(self)
        } else if sender == addButton {
            self.createLocation()
        }
    }
    
    func createLocation() {
        let alertController = UIAlertController(title:StringHelper.TitleCategory, message:StringHelper.MessageCategory, preferredStyle: .Alert)
        alertController.addTextFieldWithConfigurationHandler(nil)
        
        let okAction = UIAlertAction(title:StringHelper.AlertOK, style: UIAlertActionStyle.Default) {
            UIAlertAction in
            
            let textField = alertController.textFields![0] as UITextField
            if (textField.text!.isEmpty == true) {
                UIAlert.show(self, title:StringHelper.TitleInvalid, message:StringHelper.MessageInvalid, negativeButtonText:StringHelper.AlertOK, negativeHandler: {
                    (okAction) -> Void in
                    self.createLocation()
                })
            } else {
                self.saveLocation(textField.text!)
//                NavigationHelper.dismiss(self)
            }
        }
        
        let cancelAction = UIAlertAction(title:StringHelper.AlertCancel, style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}